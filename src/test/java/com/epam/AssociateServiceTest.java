package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dtos.AssociateDto;
import com.epam.dtos.BatchDto;
import com.epam.entities.Associate;
import com.epam.entities.Batch;
import com.epam.exceptions.AssociateException;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateServiceImpl;

@ExtendWith(MockitoExtension.class)
public class AssociateServiceTest {
	@Mock
	ModelMapper modelMapper;
	@Mock
	AssociateRepository associateRepository;
	
	@Mock
	BatchRepository batchRepository;
	
	@InjectMocks
	AssociateServiceImpl associateServiceImpl;
	
	Associate associate;
	AssociateDto associateDto;
	List<AssociateDto> associateDtoList=new ArrayList();
	List<Associate> associateList=new ArrayList();
	Batch batch;
	BatchDto batchDto;
	
	@BeforeEach
	void setup()
	{
		batch=new Batch();
		batch.setId(1);
		batch.setName("rd-java-2023");
		batch.setPractice("java");
		batch.setStartDate(new Date());
		batch.setEndDate(new Date());
		
		batchDto=new BatchDto();
		batchDto.setEndDate(new Date());
		batchDto.setEndDate(new Date());
		batchDto.setId(1);
		batchDto.setName("rd-java-2023");
		batchDto.setPractice("java");
		
		associate=new Associate();
		associate.setId(1);
		associate.setName("manvitha");
		associate.setEmail("man@gmail.com");
		associate.setGender("female");
		associate.setCollege("svec");
		associate.setStatus("inactive");
		associate.setBatch(batch);
		associateList.add(associate);
		
		associateDto=new AssociateDto();
		associateDto.setId(1);
		associateDto.setName("manvitha");
		associateDto.setEmail("man@gmail.com");
		associateDto.setGender("female");
		associateDto.setCollege("svec");
		associateDto.setStatus("inactive");
		associateDtoList.add(associateDto);
		
	}
	
	@Test 
	 void testAddAssociate()
	{
		Mockito.when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
        Mockito.when(batchRepository.save(batch)).thenReturn(batch);
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
        Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
        
        AssociateDto createdAssociateDto = associateServiceImpl.addAssociate(associateDto);
        
        assertEquals(associateDto, createdAssociateDto);
        
        Mockito.verify(modelMapper).map(associateDto, Associate.class);
        Mockito.verify(batchRepository).save(batch);
        Mockito.verify(associateRepository).save(associate);
        Mockito.verify(modelMapper).map(associate, AssociateDto.class);
	}
	
	@Test
	 void testDeleteAssociate()
	{
		Mockito.doNothing().when(associateRepository).deleteById(1);
		associateServiceImpl.deleteAssociateById(1);
		Mockito.verify(associateRepository).deleteById(1);
	}
	
	@Test
	 void testUpdateAssociate()
	{
		Mockito.when(associateRepository.findById(1)).thenReturn(Optional.of(associate));
		Mockito.when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(modelMapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		assertEquals(associateDto,associateServiceImpl.updateAssociateDetails(1, associateDto));
		Mockito.verify(associateRepository).findById(1);
		Mockito.verify(modelMapper).map(associateDto, Associate.class);
		Mockito.verify(associateRepository).save(associate);
		Mockito.verify(modelMapper).map(associate,AssociateDto.class);
	}
	
	@Test
	 void testUpdateAssociateFails()
	{
		Mockito.when(associateRepository.findById(100)).thenReturn(Optional.empty());
		assertThrows(AssociateException.class,()->associateServiceImpl.updateAssociateDetails(100, associateDto));
		Mockito.verify(associateRepository).findById(100);
	}
	@Test
	 void testGetAssociatesByGender()
	{
		Associate associateMale=new Associate();
		associateMale.setBatch(associate.getBatch());
		associateMale.setCollege(associate.getCollege());
		associateMale.setEmail(associate.getEmail());
		associateMale.setGender(associate.getGender());
		associateMale.setId(associate.getId());
		associateMale.setName(associate.getName());
		associateMale.setStatus(associate.getStatus());
		Mockito.when(associateRepository.findAllByGender("Male")).thenReturn(List.of(associate));
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		assertEquals(List.of(associateDto),associateServiceImpl.getAssociatesByGender("Male"));
		Mockito.verify(associateRepository).findAllByGender("Male");
		Mockito.verify(modelMapper).map(associate,AssociateDto.class);
		
		assertNotNull(batch.getId());
		assertNotNull(batch.getName());
		assertNotNull(batch.getPractice());
		assertNotNull(batch.getStartDate());
		assertNotNull(batch.getEndDate());
	}
		
}
