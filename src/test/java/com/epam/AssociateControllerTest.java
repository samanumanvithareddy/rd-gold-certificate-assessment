package com.epam;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.dtos.AssociateDto;
import com.epam.dtos.BatchDto;
import com.epam.service.AssociateServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class AssociateControllerTest {
	@MockBean
	AssociateServiceImpl associateService;
	
	@Autowired
	MockMvc mockMvc;
	AssociateDto associateDto;
	BatchDto batchDto;
	AssociateDto dummy;
	@BeforeEach
	void setup() {
		associateDto=new AssociateDto();
		associateDto.setId(1);
		associateDto.setName("manvitha");
		associateDto.setEmail("man@gmail.com");
		associateDto.setGender("Female");
		associateDto.setCollege("svec");
		associateDto.setStatus("inactive");
		
		dummy=new AssociateDto();
		
		batchDto=new BatchDto();
		batchDto.setId(1);
		batchDto.setName("rd");
		batchDto.setPractice("java");
		batchDto.setStartDate(Date.from(Instant.now()));
		batchDto.setEndDate(Date.from(Instant.now()));
		
	}
	
	@Test
	void testAddAssociate() throws Exception {
		Mockito.when(associateService.addAssociate(associateDto)).thenReturn(associateDto);
    	mockMvc.perform(post("/associates")
    			.contentType(MediaType.APPLICATION_JSON)
    			.content(new ObjectMapper().writeValueAsString(associateDto)))
    	.andExpect(status().isCreated())
    	.andReturn();
	}
	@Test
	void testGetAssociatesByGender() throws Exception {
		Mockito.when(associateService.getAssociatesByGender("female")).thenReturn(List.of(associateDto));
		mockMvc.perform(get("/associates/{gender}","female")).andExpect(status().isOk()).andReturn();
	}
	@Test
	void testDeleteAssociateById() throws Exception {
		Mockito.doNothing().when(associateService).deleteAssociateById(1);;
    	mockMvc.perform(delete("/associates/1"))
    	.andExpect(status().isNoContent())
    	.andReturn();
	}
	@Test//
	void testUpdateAssociateDetails() throws JsonProcessingException, Exception { 
			Mockito.when(associateService.updateAssociateDetails(1,associateDto)).thenReturn(associateDto);
			mockMvc.perform(put("/associates/1")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(new ObjectMapper().writeValueAsString(associateDto)))
	                .andExpect(status().isOk())
	                .andReturn();
		}
	
	 @Test
	    void testMethodNotValid() throws JsonProcessingException, Exception {
	    	Mockito.when(associateService.addAssociate(dummy)).thenReturn(dummy);
	    	mockMvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(dummy)))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    	
	}
	 @Test
	 void testhttpNotReadble() throws JsonProcessingException, Exception {
	    	Mockito.when(associateService.addAssociate(dummy)).thenReturn(dummy);
	    	mockMvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString("{")))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    }
}
