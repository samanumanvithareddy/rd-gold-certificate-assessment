package com.epam.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.dtos.AssociateDto;
import com.epam.entities.Associate;

public interface AssociateRepository extends JpaRepository<Associate, Integer>{
	public List<Associate> findAllByGender(String gender);
}
