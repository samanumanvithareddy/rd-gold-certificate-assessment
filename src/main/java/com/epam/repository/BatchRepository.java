package com.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.entities.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer>{

}
