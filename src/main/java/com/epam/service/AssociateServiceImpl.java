package com.epam.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dtos.AssociateDto;
import com.epam.entities.Associate;
import com.epam.exceptions.AssociateException;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AssociateServiceImpl implements AssociateService{
	
	@Autowired
	AssociateRepository associateRep;
	
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	ModelMapper modelMapper;

	@Override
	public AssociateDto addAssociate(AssociateDto associateDto) {
		log.info("AssociateService:Add Associate");
		Associate associate=modelMapper.map(associateDto, Associate.class);
		batchRepository.save(associate.getBatch());
		Associate savedAssociate=associateRep.save(associate);
		return modelMapper.map(savedAssociate,AssociateDto.class);
	}

	@Override
	public List<AssociateDto> getAssociatesByGender(String gender) {
		log.info("AssociateService:Get Associates By Gender");
		return associateRep.findAllByGender(gender).stream().map(associate->modelMapper.map(associate,AssociateDto.class)).toList();
	}

	@Override
	public void deleteAssociateById(int associateId) {
		log.info("AssociateService:Delete Associate");
		associateRep.deleteById(associateId);
		
	}

	@Override
	public AssociateDto updateAssociateDetails(int associateId, AssociateDto associateDto) {
		log.info("AssociateService:Update Associate Details");
		return associateRep.findById(associateId).map(associate->{
			Associate associate1=modelMapper.map(associateDto,Associate.class);
			associate1.setId(associateId);
			Associate updatedAssociate=associateRep.save(associate1);
			return modelMapper.map(updatedAssociate,AssociateDto.class);
			}).orElseThrow(()->new AssociateException("Id doesn't exist. Try with different id"));
	}

}
	

