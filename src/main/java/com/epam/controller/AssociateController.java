package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dtos.AssociateDto;
import com.epam.service.AssociateServiceImpl;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("associates")
@Slf4j
public class AssociateController {
	
	@Autowired
	AssociateServiceImpl associateService;
	
	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@Valid @RequestBody AssociateDto associateDto){
		log.info("Associate Controller: Post method");
		return new ResponseEntity<>(associateService.addAssociate(associateDto), HttpStatus.CREATED);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getAssociatesByGender(@PathVariable String gender){
		log.info("Associate Controller: Get method");
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void deleteAssociate(@PathVariable int id) {
		log.info("Associate Controller: Delete method");
		associateService.deleteAssociateById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AssociateDto> updateAssociateDetails(@PathVariable int id, @Valid @RequestBody AssociateDto associateDto){
		log.info("Associate Controller: Put method");
		return new ResponseEntity<>(associateService.updateAssociateDetails(id, associateDto),HttpStatus.OK);
	}
	
	

}
