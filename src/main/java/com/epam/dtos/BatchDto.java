package com.epam.dtos;

import java.util.Date;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BatchDto {
	
	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	
	@NotBlank(message="Name should not be empty. Provide a proper name")
	private String name;
	
	@NotBlank(message="Provide appropriate practice name")
	private String practice;
	
	@NotBlank(message="Provide appropriate date")
	private Date startDate;
	
	@NotBlank(message="Provide appropriate date")
	private Date endDate;

}
