package com.epam.dtos;


import com.epam.entities.Batch;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter	
@NoArgsConstructor
public class AssociateDto {
	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	
	@NotBlank(message="name should not be empty. Provide a proper name")
	private String name;
	
	@Email(message="email should be in abc@xyz.com format")
	@NotBlank(message="email should not be empty")
	private String email;
	
	@NotBlank(message="gender field should not be empty")
	@Pattern(regexp = "^(?)(Male|Female)$",message = "Gender should be either male or Female" )
	private String gender;
	
	@NotBlank(message="Provide a proper college name")
	private String college;
	
	@NotBlank(message="Provide whether your status is active or inactive")
	@Pattern(regexp = "^(?)(active|inactive)$",message = "status should be either active or inactive" )
	private String status;
	
	Batch batch;
}
